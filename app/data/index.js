const mongoose = require("mongoose");

const db = require("../models");

const vouchers = require("./CRUD_LuckyDiceCasino.vouchers.json");
const prizes = require("./CRUD_LuckyDiceCasino.prizes.json");
const roles = ["Admin", "User", "Guest"];

const initializeVoucher = async () => {
  try {
    const count = await db.voucher.countDocuments();
    if (count === 0) {
      for (const item of vouchers) {
        await db.voucher.create(item);
      }
    }
  } catch (error) {
    console.error(error);
  }
};

const initializePrize = async () => {
  try {
    const count = await db.prize.countDocuments();
    if (count === 0) {
      for (const item of prizes) {
        await db.prize.create(item);
      }
    }
  } catch (error) {
    console.error(error);
  }
};

const initializeRole = async () => {
  try {
    const count = await db.role.countDocuments();
    if (count === 0) {
      for (const item of roles) {
        await db.role.create({ name: item });
      }
    }
  } catch (error) {
    console.error(error);
  }
};

const initializeData = async () => {
  await initializeVoucher();
  await initializePrize();
  await initializeRole();
};

module.exports = initializeData;
