const bcrypt = require("bcrypt");

const db = require("../models");

//hàm xác thực đăng ký
const checkDuplicateSignup = async (req, res, next) => {
  try {
    const reqUser = req.body;

    //check username
    const existedUser = await db.user.findOne({ username: reqUser.username });
    if (existedUser) {
      return res.status(400).json({
        message: "Username already exists",
      });
    }

    //check role
    //tìm các role tương ứng các giá trị trong mảng roles
    const existedRoles = await db.role.find({ name: { $in: reqUser.roles } });
    if (existedRoles.length !== reqUser.roles.length) {
      return res.status(400).json({
        message: "One or more roles are invalid",
      });
    }
    //hợp lệ => gán lại roleId
    reqUser.roles = existedRoles.map((role) => role._id);
    next();
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//Hàm xác thực đăng nhập
const checkExistedLogin = async (req, res, next) => {
  try {
    const reqUser = req.body;
    //check user
    const existedUser = await db.user.findOne({
      username: reqUser.username,
    });
    if (!existedUser) {
      return res.status(404).json({
        message: "Invalid credentials", //ko trả cụ thể lỗi để tránh hacker
      });
    }
    //check password
    //dùng bcryct giải mã và so sánh
    const passwordIsValid = await bcrypt.compare(
      reqUser.password,
      existedUser.password
    );
    if (!passwordIsValid) {
      return res.status(404).json({
        message: "Invalid credentials", //ko trả cụ thể lỗi để tránh hacker
      });
    }
    //hợp lệ => gán user
    req.user = existedUser;
    next();
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  checkDuplicateSignup,
  checkExistedLogin
}