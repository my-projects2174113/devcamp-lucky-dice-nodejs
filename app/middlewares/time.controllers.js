const logTimeAPIRequest = (req, res, next) => {
    const currentTime = new Date().toLocaleString();
    console.log(`Current Time: ${currentTime}`);
    next();
}

module.exports = logTimeAPIRequest;