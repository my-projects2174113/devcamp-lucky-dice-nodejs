const jwt = require("jsonwebtoken");

const db = require("../models");

const ROLE_ADMIN = "Admin";

const verifyToken = async (req, res, next) => {
  try {
    //lấy access token trong header
    const accessToken = req.headers.x_success_token; //x_success_token là mã tự định nghĩa, thông thường sẽ dùng Authorization
    if (!accessToken) {
      return res.status(401).json({
        message: "Không tìm thấy access token",
      });
    }
    //lấy secretkey từ biến môi trường
    const secretKey = process.env.SECRET_KEY;
    //Giải mã token
    const decoded = await jwt.verify(accessToken, secretKey);
    if (!decoded) {
      return res.status(401).json({
        message: "access token không hợp lệ",
      });
    }
    //Tìm user theo id
    const user = await db.user.findById(decoded.id).populate("roles");
    if (!user) {
      return res.status(404).json({
        message: "user not found",
      });
    }
    //gán thông tin user vào req để các bước sau xử lý
    req.user = user;
    next();
  } catch (error) {
    console.error(error);
    if (error.name === "TokenExpiredError") {
      //check lỗi token hết hạn
      return res.status(401).json({
        message: "Access Token has expired",
      });
    }
    return res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

const checkRoleUser = async (req, res) => {
  try {
    const userRoles = req.user.roles; //lấy roles
    const check = userRoles.some((role) => role.name === ROLE_ADMIN); //dùng some để check xem có ít nhất 1 role là admin ko
    if (!check) {
      return res.status(403).json({
        message: "Bạn không có quyền truy cập tính năng này",
      });
    }
    next();
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  verifyToken,
  checkRoleUser,
};
