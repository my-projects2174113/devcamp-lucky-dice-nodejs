const logMethodAPIRequest = (req, res, next) => {
    const method = req.method;
    console.log(`Method: ${method}`);
    next();
}

module.exports = logMethodAPIRequest;