const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const PrizeHistory = new Schema(
  {
    user: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: true,
    },
    prize: {
      type: mongoose.Types.ObjectId,
      ref: "Prize",
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const PrizeHistoryModel = mongoose.model("PrizeHistory", PrizeHistory);

module.exports = PrizeHistoryModel;
