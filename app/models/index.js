const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const db = {};

db.user = require("./user.model");
db.diceHistory = require("./dice-history.model");
db.voucher = require("./voucher.model");
db.voucherHistory = require("./voucher-history.model");
db.prize = require("./prize.model");
db.prizeHistory = require("./prize-history.model");
db.role = require("./role.model");
db.refreshToken = require("./refresh-token.model");

module.exports = db;
