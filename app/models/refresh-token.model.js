const mongoose = require("mongoose");

const RefreshTokenSchema = new mongoose.Schema({
  token: {
    type: String,
    unique: true,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  expiredDate: {
    type: Date,
    required: true,
  },
});

module.exports = mongoose.model("RefreshToken", RefreshTokenSchema);

