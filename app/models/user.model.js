const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const User = new Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
    },
    firstname: {
      type: String,
      required: true,
    },
    lastname: {
      type: String,
      required: true,
    },
    password: {
      //trường này tự thêm để đăng nhập
      type: String,
      required: true,
    },
    roles: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Role",
      },
    ],
  },
  {
    timestamps: true,
  }
);

const UserModel = mongoose.model("User", User);

module.exports = UserModel;
