const express = require('express');

const {
  verifyToken,
  checkRoleUser,
} = require("../middlewares/author.middleware");

const {
  createPrizeHistory,
  getAllPrizeHistories,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById,
} = require('../controllers/prize-history.controller');

const PrizeHistoryRouter = express.Router();

PrizeHistoryRouter.post('/', [verifyToken, checkRoleUser], createPrizeHistory);

PrizeHistoryRouter.get('/', [verifyToken, checkRoleUser], getAllPrizeHistories);

PrizeHistoryRouter.get('/:prizeHistoryId', [verifyToken, checkRoleUser], getPrizeHistoryById);

PrizeHistoryRouter.put('/:prizeHistoryId', [verifyToken, checkRoleUser], updatePrizeHistoryById);

PrizeHistoryRouter.delete('/:prizeHistoryId', [verifyToken, checkRoleUser], deletePrizeHistoryById);

module.exports = PrizeHistoryRouter;