const express = require("express");

const {
  verifyToken,
  checkRoleUser,
} = require("../middlewares/author.middleware");

const {
  createPrize,
  getAllPrizes,
  getPrizeById,
  updatePrizeById,
  deletePrizeById,
} = require("../controllers/prize.controller");

const PrizeRouter = express.Router();

PrizeRouter.post('/', [verifyToken, checkRoleUser], createPrize);

PrizeRouter.get('/', [verifyToken, checkRoleUser], getAllPrizes);

PrizeRouter.get('/:prizeId', [verifyToken, checkRoleUser], getPrizeById);

PrizeRouter.put('/:prizeId', [verifyToken, checkRoleUser], updatePrizeById);

PrizeRouter.delete('/:prizeId', [verifyToken, checkRoleUser], deletePrizeById);

module.exports = PrizeRouter;