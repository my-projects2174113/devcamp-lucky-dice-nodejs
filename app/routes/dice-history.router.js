const express = require("express");

const {
  verifyToken,
  checkRoleUser,
} = require("../middlewares/author.middleware");

const {
  createDiceHistory,
  getAllDiceHistories,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById,
} = require("../controllers/dice-history.controller");

const DiceHistoryRouter = express.Router();

DiceHistoryRouter.post("/", [verifyToken, checkRoleUser], createDiceHistory);

DiceHistoryRouter.get("/", [verifyToken, checkRoleUser], getAllDiceHistories);

DiceHistoryRouter.get("/:diceHistoryId", [verifyToken, checkRoleUser], getDiceHistoryById);

DiceHistoryRouter.put("/:diceHistoryId", [verifyToken, checkRoleUser], updateDiceHistoryById);

DiceHistoryRouter.delete("/:diceHistoryId", [verifyToken, checkRoleUser],  deleteDiceHistoryById);

module.exports = DiceHistoryRouter;
