const express = require("express");

const {
  verifyToken,
  checkRoleUser,
} = require("../middlewares/author.middleware");

const {
  createVoucherHistory,
  getAllVoucherHistories,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById,
} = require("../controllers/voucher-history.controller");

const VoucherHistoryRouter = express.Router();

VoucherHistoryRouter.post("/", [verifyToken, checkRoleUser], createVoucherHistory);

VoucherHistoryRouter.get("/", [verifyToken, checkRoleUser], getAllVoucherHistories);

VoucherHistoryRouter.get("/:voucherHistoryId", [verifyToken, checkRoleUser], getVoucherHistoryById);

VoucherHistoryRouter.put("/:voucherHistoryId", [verifyToken, checkRoleUser], updateVoucherHistoryById);

VoucherHistoryRouter.delete("/:voucherHistoryId", [verifyToken, checkRoleUser], deleteVoucherHistoryById);

module.exports = VoucherHistoryRouter;
