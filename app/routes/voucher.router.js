const express = require("express");

const {
  verifyToken,
  checkRoleUser,
} = require("../middlewares/author.middleware");

const {
  createVoucher,
  getAllVouchers,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById,
} = require("../controllers/voucher.controller");

const VoucherRouter = express.Router();

VoucherRouter.post('/', [verifyToken, checkRoleUser], createVoucher);

VoucherRouter.get('/', [verifyToken, checkRoleUser], getAllVouchers);

VoucherRouter.get('/:voucherId', [verifyToken, checkRoleUser], getVoucherById);

VoucherRouter.put('/:voucherId', [verifyToken, checkRoleUser], updateVoucherById);

VoucherRouter.delete('/:voucherId', [verifyToken, checkRoleUser], deleteVoucherById);

module.exports = VoucherRouter;