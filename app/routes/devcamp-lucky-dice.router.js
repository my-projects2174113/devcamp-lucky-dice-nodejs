const express = require("express");

const { verifyToken } = require("../middlewares/author.middleware");

const {
  getNewDice,
  getAllDiceHistories,
  getAllPrizeHistories,
  getAllVoucherHistories,
  getInfoUserLogin,
} = require("../controllers/devcamp-lucky-dice.controller");

const DevcampLuckyDiceRouter = express.Router();

DevcampLuckyDiceRouter.post("/dice", verifyToken, getNewDice);

DevcampLuckyDiceRouter.get("/dice-histories", getAllDiceHistories);

DevcampLuckyDiceRouter.get("/prize-histories", getAllPrizeHistories);

DevcampLuckyDiceRouter.get("/voucher-histories", getAllVoucherHistories);

DevcampLuckyDiceRouter.get("/user-info", verifyToken, getInfoUserLogin);

module.exports = DevcampLuckyDiceRouter;
