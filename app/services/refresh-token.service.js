const { v4: uuidv4 } = require("uuid");
const db = require("../models");

const createRefreshToken = async (user) => {
  //Tạo thời điểm hết hạn cho token
  //Lấy thời điểm hiện tại
  let expiredAt = new Date();

  //Cộng thêm thời gian tới hạn
  expiredAt.setSeconds(
    expiredAt.getSeconds() + parseInt(process.env.EXPIRES_TIME_REFRESH)
  );

  //tạo refreshtoken
  const newToken = uuidv4();

  //thêm vào db
  const refreshToken = await db.refreshToken.create({
    token: newToken,
    user: user._id,
    expiredDate: expiredAt,
  });

  return refreshToken.token;
};

module.exports = createRefreshToken;