const mongoose = require("mongoose");

const db = require("../models");

const randomNumber = () => {
  return (random = Math.ceil(Math.random() * 6));
};
//hàm lấy dice mới
const getNewDice = async (req, res) => {
  try {
    //B1: Collect
    const reqUser = req.user;
    //B2: Validate
    if (!reqUser.username) {
      return res.status(400).json({
        message: "username is required",
      });
    }
    if (!reqUser.firstname) {
      return res.status(400).json({
        message: "firstname is required",
      });
    }
    if (!reqUser.lastname) {
      return res.status(400).json({
        message: "lastname is required",
      });
    }
    //B3: Handle
    // //tìm user(đoạn này bỏ qua vì đã làm ở author middleware)
    // let reqUser = await db.user.findOne({ username: reqUser.username });
    // if (!reqUser) {
    //   //nếu ko tồn tại trong hệ thống
    //   let newUser = {
    //     username: reqUser.username,
    //     firstname: reqUser.firstname,
    //     lastname: reqUser.lastname,
    //   };
    //   reqUser = await db.user.create(newUser); //tạo mới user và gán vào foundUser tiện dùng lại
    // }
    //tạo mới dicehistory
    let newDiceHistory = {
      user: reqUser._id,
      dice: randomNumber(),
    };
    const createdDiceHistory = await db.diceHistory.create(newDiceHistory);
    //tạo kết quả trả về
    let result = {
      dice: createdDiceHistory.dice,
      voucher: null,
      prize: null,
    };
    if (createdDiceHistory.dice < 3) {
      //nếu dice < 3
      return res.status(201).json(result);
    }
    //nếu dice >= 3
    //lấy random 1 voucher
    let countVoucher = await db.voucher.countDocuments(); //đếm số lượng voucher
    let randomVoucher = Math.floor(Math.random() * countVoucher); //lấy random 1 voucher(dưới dạng index)
    result.voucher = await db.voucher.findOne().skip(randomVoucher); //dùng findOne lấy voucher đầu tiên sau khi bỏ qua randomVoucher đầu, gán cho result
    //thêm mới voucherhistory
    let newVoucherHistory = {
      user: reqUser._id,
      voucher: result.voucher._id,
    };
    await db.voucherHistory.create(newVoucherHistory); //thêm mới voucherHistory(ko cần kqua trả về)
    //Lấy 3 lần giao xúc xắc gần nhất của user
    const recentDiceHistories = await db.diceHistory
      .find({
        user: reqUser._id,
      })
      .sort({ createdAt: -1 }) // Sắp xếp theo thời gian giảm dần (tức là mới nhất lên trên)
      .limit(3); // Giới hạn kết quả trả về cho 3 bản ghi đầu tiên
    //dùng every kiểm tra thuộc tính dice cả 3 lần gần nhất có lớn hơn 3 ko
    let checkDice = recentDiceHistories.every(
      (diceHistory) => diceHistory.dice > 3
    );

    if (checkDice) {
      //nếu ltiep đc 3 lần > 3 => có thưởng
      //lấy random 1 prize
      let countPrize = await db.prize.countDocuments(); //đếm số lượng prize
      let randomPrize = Math.floor(Math.random() * countPrize); //lấy random 1 prize(dưới dạng index)
      result.prize = await db.prize.findOne().skip(randomPrize); //dùng findOne lấy prize đầu tiên sau khi bỏ qua randomPrize đầu, gán cho result
      //thêm mới prizeHistory
      let newPrizeHistory = {
        user: reqUser._id,
        prize: result.prize._id,
      };
      await db.prizeHistory.create(newPrizeHistory); //ko cần trả về
    }

    //trả về cho client
    res.status(201).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//Hàm lấy lịch sử xúc xắc của 1 user
const getAllDiceHistories = async (req, res) => {
  try {
    //B1: Thu thập
    const username = req.query.username; //lấy querystring
    //B2: Validate
    if (!username) {
      return res.status(400).json({
        message: "username is required",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findOne({ username });
    if(!foundUser) {
      return res.status(404).json({
        message: "User not found"
      })
    }
    //tạo dkien lọc
    const condition = {
      user: { $eq: foundUser ? foundUser._id : null }, //null sẽ ko bị mongoose bỏ qua
    };
    //get all or get dice histories of user
    const allDiceHistories = await db.diceHistory
      .find(condition)
      .populate("user");
    //phản hồi
    res.status(200).json({
      message: "Get Dice Histories Successfully",
      data: allDiceHistories
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

//Hàm lấy lịch sử phần thưởng của 1 user
const getAllPrizeHistories = async (req, res) => {
  try {
    //B1: Thu thập
    const username = req.query.username; //lấy querystring
    //B2: Validate
    if (!username) {
      return res.status(400).json({
        message: "username is required",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findOne({ username });
    if(!foundUser) {
      return res.status(404).json({
        message: "User not found"
      })
    }
    //tạo dkien lọc
    const condition = {
      user: { $eq: foundUser ? foundUser._id : null }, //null sẽ ko bị mongoose bỏ qua
    };
    //get all or get prize histories of user
    const allPrizeHistories = await db.prizeHistory
      .find(condition)
      .populate("user prize");
    //phản hồi
    res.status(200).json({
      message: "Get Prize Histories Successfully",
      data: allPrizeHistories
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

//Hàm lấy lịch sử voucher của 1 user
const getAllVoucherHistories = async (req, res) => {
  try {
    //B1: Thu thập
    const username = req.query.username; //lấy querystring
    //B2: Validate
    if (!username) {
      return res.status(400).json({
        message: "username is required",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findOne({ username });
    if(!foundUser) {
      return res.status(404).json({
        message: "User not found"
      })
    }
    //tạo dkien lọc
    const condition = {
      user: { $eq: foundUser ? foundUser._id : null }, //null sẽ ko bị mongoose bỏ qua
    };
    //get all or get voucher histories of user
    const allVoucherHistories = await db.voucherHistory
      .find(condition)
      .populate("user voucher");
    //phản hồi
    res.status(200).json({
      message: "Get Voucher Histories Successfully",
      data: allVoucherHistories
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

//Hàm lấy info user login
const getInfoUserLogin = async (req, res) => {
  try {
    const user = req.user;
    const userInfo = {
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
    };
    res.status(200).json(userInfo);
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
  getNewDice,
  getAllDiceHistories,
  getAllPrizeHistories,
  getAllVoucherHistories,
  getInfoUserLogin,
};
