const randomNumber = (req, res) => {
    const random = Math.ceil(Math.random() * 6);
    res.status(200).json({
        message: 'Get random number succesfully',
        data: random,
    })
}

module.exports = randomNumber;