const bcryct = require("bcrypt");
const jwt = require("jsonwebtoken");

const createRefreshToken = require("../services/refresh-token.service");
const db = require("../models");

// Hàm kiểm tra định dạng của username
const isValidUsername = (username) => {
  const usernameRegex = /^[a-zA-Z0-9_-]{3,16}$/;
  return usernameRegex.test(username);
};

// Hàm kiểm tra định dạng của name
const isValidName = (name) => {
  if (typeof name !== "string" || name.length < 1) {
    return false;
  }
  return true;
};

// Hàm kiểm tra định dạng của password
const isValidPassword = (password) => {
  if (typeof password !== "string" || password.length < 6) {
    return false;
  }
  return true;
};

const signup = async (req, res) => {
  try {
    //B1: Collect
    const reqUser = req.body;
    //B2: Validate
    if (
      !isValidUsername(reqUser.username) ||
      !isValidName(reqUser.firstname) ||
      !isValidName(reqUser.lastname) ||
      !isValidPassword(reqUser.password)
    ) {
      return res.status(400).json({
        message: "Invalid data format",
      });
    }
    //B3: Handle
    const newUser = {
      username: reqUser.username,
      firstname: reqUser.firstname,
      lastname: reqUser.lastname,
      password: bcryct.hashSync(reqUser.password, 8),
      roles: reqUser.roles || [],
    };
    //tạo mới và trả về
    await db.user.create(newUser);
    res.status(201).json({
      message: "Register successfully",
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

const login = async (req, res) => {
  try {
    //B1: Collect
    const existedUser = req.user;
    //B2: Validate
    //B3: Handle
    //Lấy khóa bảo mật từ biến môi trường
    const secretKey = process.env.SECRET_KEY;

    //dùng jwt tạo và trả về token
    //Tạo accesstoken
    const accessToken = await jwt.sign(
      {
        id: existedUser._id, //id user
      },
      secretKey,
      {
        expiresIn: process.env.EXPIRES_TIME_ACCESS, //thời hạn
      }
    );
    //tạo refreshtoken
    const refreshToken = await createRefreshToken(existedUser);

    //phản hồi
    res.status(200).json({
      accessToken,
      refreshToken,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//hàm refreshtoken
const refreshToken = async (req, res) => {
  try {
    //lấy refreshtoken trong body
    const refreshToken = req.body.refreshToken;

    //check xem có hay ko
    if (!refreshToken) {
      return res.status(403).json({
        message: "Refresh Token is required",
      });
    }

    //tìm token trong db
    const refreshTokenObj = await db.refreshToken.findOne({
      token: refreshToken,
    });

    if (!refreshTokenObj) {
      return res.status(403).json({
        message: "Refresh Token not found",
      });
    }

    //kiểm tra thời hạn token
    if (refreshTokenObj.expiredDate.getTime() < new Date().getTime()) {
      return res.status(401).json({
        message: "Refresh token has expired",
      });
    }

    //hợp lệ rồi thì tạo mới accesstoken
    const secretKey = process.env.SECRET_KEY; //lấy khóa bảo mật từ biến môi trường
    //tạo accesstoken mới
    const newAccessToken = await jwt.sign(
      {
        id: refreshTokenObj.user,
      },
      secretKey,
      {
        expiresIn: process.env.EXPIRES_TIME_ACCESS,
      }
    );

    //phản hồi
    res.status(200).json({
      accessToken: newAccessToken,
      refreshToken: refreshTokenObj.token,
    });
  } catch (error) {
    console.error(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

module.exports = {
  signup,
  login,
  refreshToken
};
