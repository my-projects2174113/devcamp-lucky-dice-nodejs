const mongoose = require("mongoose");

const db = require('../models');

const createVoucherHistory = async (req, res) => {
  try {
    //B1: Thu thập
    const reqVoucherHistory = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(reqVoucherHistory.user)) {
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(reqVoucherHistory.voucher)) {
      return res.status(400).json({
        message: "voucher is invalid",
      });
    }
    //B3: Xử lý
    //tìm user và voucher
    const foundUser = await db.user.findById(reqVoucherHistory.user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    const foundVoucher = await db.voucher.findById(reqVoucherHistory.voucher);
    if (!foundVoucher) {
      return res.status(404).json({
        message: "Voucher not found",
      });
    }
    //thêm mới
    let newVoucherHistory = {
      user: reqVoucherHistory.user,
      voucher: reqVoucherHistory.voucher,
    };
    const createdVoucherHistory = await db.voucherHistory.create(
      newVoucherHistory
    );
    await createdVoucherHistory.populate("user voucher");
    //phản hồi
    res.status(201).json({
      message: "Create Voucher History successfully",
      data: createdVoucherHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getAllVoucherHistories = async (req, res) => {
  try {
    //B1: Thu thập
    const user = req.query.user; //lấy querystring
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(user) && user !== undefined) {
      //nếu ko truyền query thì lấy tất cả
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findById(user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    //tạo dkien lọc
    const condition = {};
    if (user) {
      condition.user = user;
      //$regex chỉ hợp lý khi tìm 1 phần của chuỗi(ví dụ tìm "John", kqua có thể là "John Doe", "John Smith")
    }
    //get all or get voucher histories of user
    const allVoucherHistories = await db.voucherHistory.find(
      condition
    ).populate("user voucher");
    //phản hồi
    res.status(200).json({
      message: "Get All Voucher Histories successfully",
      data: allVoucherHistories,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getVoucherHistoryById = async (req, res) => {
  try {
    //B1: Thu thập
    const voucherHistoryId = req.params.voucherHistoryId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
      return res.status(400).json({
        message: "voucherHistoryId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const foundVoucherHistory = await db.voucherHistory.findById(
      voucherHistoryId
    ).populate("user voucher");
    if (!foundVoucherHistory) {
      return res.status(404).json({
        message: "Voucher History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Get Voucher History By Id successfully",
      data: foundVoucherHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const updateVoucherHistoryById = async (req, res) => {
  try {
    //B1: Thu thập
    const voucherHistoryId = req.params.voucherHistoryId;
    const reqVoucherHistory = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
      return res.status(400).json({
        message: "voucherHistoryId is invalid",
      });
    }
    if (
      !mongoose.Types.ObjectId.isValid(reqVoucherHistory.user) &&
      reqVoucherHistory.user !== undefined
    ) {
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    if (
      !mongoose.Types.ObjectId.isValid(reqVoucherHistory.voucher) &&
      reqVoucherHistory.voucher !== undefined
    ) {
      return res.status(400).json({
        message: "voucher is invalid",
      });
    }
    //B3: Xử lý
    //tìm user và voucher
    const foundUser = await db.user.findById(reqVoucherHistory.user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    const foundVoucher = await db.voucher.findById(reqVoucherHistory.voucher);
    if (!foundVoucher) {
      return res.status(404).json({
        message: "Voucher not found",
      });
    }
    //tìm và update
    let patchVoucherHistory = {
      user: reqVoucherHistory.user,
      voucher: reqVoucherHistory.voucher,
    };
    const updatedVoucherHistory = await db.voucherHistory.findByIdAndUpdate(
      voucherHistoryId,
      patchVoucherHistory,
      { new: true }
    ).populate("user voucher");
    if (!updatedVoucherHistory) {
      return res.status(404).json({
        message: "Voucher History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Update Voucher History By Id successfully",
      data: updatedVoucherHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const deleteVoucherHistoryById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const voucherHistoryId = req.params.voucherHistoryId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
      return res.status(400).json({
        message: "voucherHistoryId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const deletedVoucherHistory = await db.voucherHistory.findByIdAndDelete(
      voucherHistoryId
    ).populate("user voucher");
    if (!deletedVoucherHistory) {
      return res.status(404).json({
        message: "Voucher History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Delete Voucher History By Id successfully",
      data: deletedVoucherHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
  createVoucherHistory,
  getAllVoucherHistories,
  getVoucherHistoryById,
  updateVoucherHistoryById,
  deleteVoucherHistoryById,
};
