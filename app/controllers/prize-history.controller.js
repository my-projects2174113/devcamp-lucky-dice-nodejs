const mongoose = require("mongoose");

const db = require('../models');

const createPrizeHistory = async (req, res) => {
  try {
    //B1: Thu thập
    const reqPrizeHistory = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(reqPrizeHistory.user)) {
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    if (!mongoose.Types.ObjectId.isValid(reqPrizeHistory.prize)) {
      return res.status(400).json({
        message: "prize is invalid",
      });
    }
    //B3: Xử lý
    //tìm user và prize
    const foundUser = await db.user.findById(reqPrizeHistory.user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    const foundPrize = await db.prize.findById(reqPrizeHistory.prize);
    if (!foundPrize) {
      return res.status(404).json({
        message: "Prize not found",
      });
    }
    //thêm mới
    let newPrizeHistory = {
      user: reqPrizeHistory.user,
      prize: reqPrizeHistory.prize,
    };
    const createdPrizeHistory = await db.prizeHistory.create(newPrizeHistory);
    await createdPrizeHistory.populate("user prize");
    //phản hồi
    res.status(201).json({
      message: "Create Prize History successfully",
      data: createdPrizeHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getAllPrizeHistories = async (req, res) => {
  try {
    //B1: Thu thập
    const user = req.query.user; //lấy querystring
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(user) && user !== undefined) {
      //nếu ko truyền query thì lấy tất cả
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findById(user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    //tạo dkien lọc
    const condition = {};
    if (user) {
      condition.user = user;
      //$regex chỉ hợp lý khi tìm 1 phần của chuỗi(ví dụ tìm "John", kqua có thể là "John Doe", "John Smith")
    }
    //get all or get prize histories of user
    const allPrizeHistories = await db.prizeHistory.find(condition).populate(
      "user prize"
    );
    //phản hồi
    res.status(200).json({
      message: "Get All Prize Histories successfully",
      data: allPrizeHistories,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getPrizeHistoryById = async (req, res) => {
  try {
    //B1: Thu thập
    const prizeHistoryId = req.params.prizeHistoryId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
      return res.status(400).json({
        message: "prizeHistoryId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const foundPrizeHistory = await db.prizeHistory.findById(
      prizeHistoryId
    ).populate("user prize");
    if (!foundPrizeHistory) {
      return res.status(404).json({
        message: "Prize History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Get Prize History By Id successfully",
      data: foundPrizeHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const updatePrizeHistoryById = async (req, res) => {
  try {
    //B1: Thu thập
    const prizeHistoryId = req.params.prizeHistoryId;
    const reqPrizeHistory = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
      return res.status(400).json({
        message: "prizeHistoryId is invalid",
      });
    }
    if (
      !mongoose.Types.ObjectId.isValid(reqPrizeHistory.user) &&
      reqPrizeHistory.user !== undefined
    ) {
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    if (
      !mongoose.Types.ObjectId.isValid(reqPrizeHistory.prize) &&
      reqPrizeHistory.prize !== undefined
    ) {
      return res.status(400).json({
        message: "prize is invalid",
      });
    }
    //B3: Xử lý
    //tìm user và prize
    const foundUser = await db.user.findById(reqPrizeHistory.user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    const foundPrize = await db.prize.findById(reqPrizeHistory.prize);
    if (!foundPrize) {
      return res.status(404).json({
        message: "Prize not found",
      });
    }
    //tìm và update
    let patchPrizeHistory = {
      user: reqPrizeHistory.user,
      prize: reqPrizeHistory.prize,
    };
    const updatedPrizeHistory = await db.prizeHistory.findByIdAndUpdate(
      prizeHistoryId,
      patchPrizeHistory,
      { new: true }
    ).populate("user prize");
    if (!updatedPrizeHistory) {
      return res.status(404).json({
        message: "Prize History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Update Prize History By Id successfully",
      data: updatedPrizeHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const deletePrizeHistoryById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const prizeHistoryId = req.params.prizeHistoryId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
      return res.status(400).json({
        message: "prizeHistoryId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const deletedPrizeHistory = await db.prizeHistory.findByIdAndDelete(
      prizeHistoryId
    ).populate("user prize");
    if (!deletedPrizeHistory) {
      return res.status(404).json({
        message: "Prize History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Delete Prize History By Id successfully",
      data: deletedPrizeHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
  createPrizeHistory,
  getAllPrizeHistories,
  getPrizeHistoryById,
  updatePrizeHistoryById,
  deletePrizeHistoryById,
};
