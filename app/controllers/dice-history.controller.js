const mongoose = require("mongoose");

const db = require('../models');

const createDiceHistory = async (req, res) => {
  try {
    //B1: Thu thập
    const reqDiceHistory = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(reqDiceHistory.user)) {
      //bắt buộc history phải có user
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    if (!reqDiceHistory.dice && reqDiceHistory.dice !== 0) {
      return res.status(400).json({
        message: "dice is required",
      });
    }
    if (
      !Number.isInteger(reqDiceHistory.dice) ||
      reqDiceHistory.dice < 1 ||
      reqDiceHistory.dice > 6
    ) {
      return res.status(400).json({
        message: "dice is invalid",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findById(reqDiceHistory.user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    //thêm mới
    let newDiceHistory = {
      user: reqDiceHistory.user,
      dice: reqDiceHistory.dice,
    };
    const createdDiceHistory = await db.diceHistory.create(newDiceHistory);
    await createdDiceHistory.populate("user");
    //phản hồi
    res.status(201).json({
      message: "Create Dice History successfully",
      data: createdDiceHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getAllDiceHistories = async (req, res) => {
  try {
    //B1: Thu thập
    const user = req.query.user; //lấy querystring
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(user) && user !== undefined) {
      //nếu ko truyền query thì lấy tất cả
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findById(user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    //tạo dkien lọc
    const condition = {};
    if (user) {
      condition.user = user;
      //$regex chỉ hợp lý khi tìm 1 phần của chuỗi(ví dụ tìm "John", kqua có thể là "John Doe", "John Smith")
    }
    //get all or get dice histories of user
    const allDiceHistories = await db.diceHistory.find(condition).populate(
      "user"
    );
    //phản hồi
    res.status(200).json({
      message: "Get All Dice Histories successfully",
      data: allDiceHistories,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getDiceHistoryById = async (req, res) => {
  try {
    //B1: Thu thập
    const diceHistoryId = req.params.diceHistoryId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
      return res.status(400).json({
        message: "diceHistoryId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const foundDiceHistory = await db.diceHistory.findById(
      diceHistoryId
    ).populate("user");
    if (!foundDiceHistory) {
      return res.status(404).json({
        message: "Dice History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Get Dice History By Id successfully",
      data: foundDiceHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const updateDiceHistoryById = async (req, res) => {
  try {
    //B1: Thu thập
    const diceHistoryId = req.params.diceHistoryId;
    const reqDiceHistory = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
      return res.status(400).json({
        message: "diceHistoryId is invalid",
      });
    }
    if (
      !mongoose.Types.ObjectId.isValid(reqDiceHistory.user) &&
      reqDiceHistory.user !== undefined
    ) {
      //bắt buộc history phải có user
      //kiểm tra có khớp với định dạng objectId ko
      return res.status(400).json({
        message: "user is invalid",
      });
    }
    if (
      !reqDiceHistory.dice &&
      reqDiceHistory.dice !== 0 &&
      reqDiceHistory.dice !== undefined
    ) {
      return res.status(400).json({
        message: "dice is required",
      });
    }
    if (
      !Number.isInteger(reqDiceHistory.dice) ||
      reqDiceHistory.dice < 1 ||
      reqDiceHistory.dice > 6
    ) {
      return res.status(400).json({
        message: "dice is invalid",
      });
    }
    //B3: Xử lý
    //tìm user
    const foundUser = await db.user.findById(reqDiceHistory.user);
    if (!foundUser) {
      return res.status(404).json({
        message: "User not found",
      });
    }
    //tìm và update
    let patchDiceHistory = {
      user: reqDiceHistory.user,
      dice: reqDiceHistory.dice,
    };
    const updatedDiceHistory = await db.diceHistory.findByIdAndUpdate(
      diceHistoryId,
      patchDiceHistory,
      { new: true }
    ).populate("user");
    if (!updatedDiceHistory) {
      return res.status(404).json({
        message: "Dice History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Update Dice History By Id successfully",
      data: updatedDiceHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const deleteDiceHistoryById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
      return res.status(400).json({
        message: "diceHistoryId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const deletedDiceHistory = await db.diceHistory.findByIdAndDelete(
      diceHistoryId
    ).populate("user");
    if (!deletedDiceHistory) {
      return res.status(404).json({
        message: "Dice History not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Delete Dice History By Id successfully",
      data: deletedDiceHistory,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
  createDiceHistory,
  getAllDiceHistories,
  getDiceHistoryById,
  updateDiceHistoryById,
  deleteDiceHistoryById,
};
