const mongoose = require("mongoose");

const db = require('../models');

const createPrize = async (req, res) => {
  try {
    //B1: Thu thập
    const reqPrize = req.body;
    //B2: Validate
    if (!reqPrize.name) {
      return res.status(400).json({
        message: "name is required",
      });
    }
    //B3: Xử lý
    //thêm mới
    let newPrize = {
      name: reqPrize.name,
      description: reqPrize.description,
    };
    const createdPrize = await db.prize.create(newPrize);
    //phản hồi
    res.status(201).json({
      message: "Create Prize successfully",
      data: createdPrize,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "name is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getAllPrizes = async (req, res) => {
  try {
    //B1: Thu thập
    //B2: Validate
    //B3: Xử lý
    //thêm mới
    const allPrizes = await db.prize.find();
    //phản hồi
    res.status(200).json({
      message: "Get All Prizes successfully",
      data: allPrizes,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getPrizeById = async (req, res) => {
  try {
    //B1: Thu thập
    const prizeId = req.params.prizeId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
      return res.status(400).json({
        message: "prizeId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const foundPrize = await db.prize.findById(prizeId);
    if (!foundPrize) {
      return res.status(404).json({
        message: "Prize not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Get Prize By Id successfully",
      data: foundPrize,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const updatePrizeById = async (req, res) => {
  try {
    //B1: Thu thập
    const prizeId = req.params.prizeId;
    const reqPrize = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
      return res.status(400).json({
        message: "prizeId is invalid",
      });
    }
    if (!reqPrize.name && reqPrize.name !== undefined) {
      return res.status(400).json({
        message: "name is required",
      });
    }
    //B3: Xử lý
    //tìm và update
    let patchPrize = {
      name: reqPrize.name,
      description: reqPrize.description,
    };
    const updatedPrize = await db.prize.findByIdAndUpdate(
      prizeId,
      patchPrize,
      {
        new: true,
      }
    );
    if (!updatedPrize) {
      return res.status(404).json({
        message: "Prize not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Update Prize By Id successfully",
      data: updatedPrize,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "name is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const deletePrizeById = async (req, res) => {
  try {
    //B1: Thu thập
    const prizeId = req.params.prizeId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
      return res.status(400).json({
        message: "prizeId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const deletedPrize = await db.prize.findByIdAndDelete(prizeId);
    if (!deletedPrize) {
      return res.status(404).json({
        message: "Prize not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Delete Prize By Id successfully",
      data: deletedPrize,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
  createPrize,
  getAllPrizes,
  getPrizeById,
  updatePrizeById,
  deletePrizeById,
};
