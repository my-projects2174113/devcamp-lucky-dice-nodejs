const mongoose = require("mongoose");

const db = require('../models');

const createVoucher = async (req, res) => {
  try {
    //B1: Thu thập
    const reqVoucher = req.body;
    //B2: Validate
    if (!reqVoucher.code) {
      return res.status(400).json({
        message: "code is required",
      });
    }
    if (!reqVoucher.discount && reqVoucher.discount !== 0) {
      return res.status(400).json({
        message: "discount is required",
      });
    }
    if (
      !Number.isFinite(reqVoucher.discount) ||
      reqVoucher.discount < 0 ||
      reqVoucher.discount > 100
    ) {
      return res.status(400).json({
        message: "discount is invalid",
      });
    }
    //B3: Xử lý
    //thêm mới
    let newVoucher = {
      code: reqVoucher.code,
      discount: reqVoucher.discount,
      note: reqVoucher.note,
    };
    const createdVoucher = await db.voucher.create(newVoucher);
    //phản hồi
    res.status(201).json({
      message: "Create Voucher successfully",
      data: createdVoucher,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "code is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getAllVouchers = async (req, res) => {
  try {
    //B1: Thu thập
    //B2: Validate
    //B3: Xử lý
    //thêm mới
    const allVouchers = await db.voucher.find();
    //phản hồi
    res.status(200).json({
      message: "Get All Vouchers successfully",
      data: allVouchers,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getVoucherById = async (req, res) => {
  try {
    //B1: Thu thập
    const voucherId = req.params.voucherId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
      return res.status(400).json({
        message: "voucherId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const foundVoucher = await db.voucher.findById(voucherId);
    if (!foundVoucher) {
      return res.status(404).json({
        message: "Voucher not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Get Voucher By Id successfully",
      data: foundVoucher,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const updateVoucherById = async (req, res) => {
  try {
    //B1: Thu thập
    const voucherId = req.params.voucherId;
    const reqVoucher = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
      return res.status(400).json({
        message: "voucherId is invalid",
      });
    }
    if (!reqVoucher.code && reqVoucher.code !== undefined) {
      return res.status(400).json({
        message: "code is required",
      });
    }
    if (
      !reqVoucher.discount &&
      reqVoucher.discount !== 0 &&
      reqVoucher.discount !== undefined
    ) {
      return res.status(400).json({
        message: "discount is required",
      });
    }
    if (
      !Number.isFinite(reqVoucher.discount) ||
      reqVoucher.discount < 0 ||
      reqVoucher.discount > 100
    ) {
      return res.status(400).json({
        message: "discount is invalid",
      });
    }
    //B3: Xử lý
    //tìm và update
    let patchVoucher = {
      code: reqVoucher.code,
      discount: reqVoucher.discount,
      note: reqVoucher.note,
    };
    const updatedVoucher = await db.voucher.findByIdAndUpdate(
      voucherId,
      patchVoucher,
      {
        new: true,
      }
    );
    if (!updatedVoucher) {
      return res.status(404).json({
        message: "Voucher not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Update Voucher By Id successfully",
      data: updatedVoucher,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "code is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const deleteVoucherById = async (req, res) => {
  try {
    //B1: Thu thập
    const voucherId = req.params.voucherId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
      return res.status(400).json({
        message: "voucherId is invalid",
      });
    }
    //B3: Xử lý
    //tìm
    const deletedVoucher = await db.voucher.findByIdAndDelete(voucherId);
    if (!deletedVoucher) {
      return res.status(404).json({
        message: "Voucher not found",
      });
    }
    //phản hồi
    res.status(200).json({
      message: "Delete Voucher By Id successfully",
      data: deletedVoucher,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
  createVoucher,
  getAllVouchers,
  getVoucherById,
  updateVoucherById,
  deleteVoucherById,
};
