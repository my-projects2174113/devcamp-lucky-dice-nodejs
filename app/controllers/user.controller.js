const mongoose = require("mongoose");
const bcrypt = require("bcrypt"); //tạo token

const db = require('../models');

const createUser = async (req, res) => {
  try {
    //B1: Thu thập
    const reqUser = req.body;
    //B2: Validate
    if (!reqUser.username) {
      return res.status(400).json({
        message: "username is required",
      });
    }
    if (!reqUser.firstname) {
      return res.status(400).json({
        message: "firstname is required",
      });
    }
    if (!reqUser.lastname) {
      return res.status(400).json({
        message: "lastname is required",
      });
    }
    //B3: Xử lý
    //thêm mới
    let newUser = {
      username: reqUser.username,
      firstname: reqUser.firstname,
      lastname: reqUser.lastname,
      password: bcrypt.hashSync(reqUser.password, 8),//cái này tự thêm để login
      roles: reqUser.roles || [],
    };
    const createdUser = await db.user.create(newUser);
    //phản hồi
    res.status(201).json({
      message: "Create User successfully",
      data: createdUser,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "username is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getAllUsers = async (req, res) => {
  try {
    //B1: Thu thập
    //B2: Validate
    //B3: Xử lý
    //thêm mới
    const allUsers = await db.user.find();
    //phản hồi
    res.status(200).json({
      message: "Get All Users successfully",
      data: allUsers,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const getUserById = async (req, res) => {
  try {
    //B1: Thu thập
    const userId = req.params.userId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: 'userId is invalid'
      })
    }
    //B3: Xử lý
    //tìm
    const foundUser = await db.user.findById(userId);
    if (!foundUser) {
      return res.status(404).json({
        message: 'User not found'
      })
    }
    //phản hồi
    res.status(200).json({
      message: "Get User By Id successfully",
      data: foundUser,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const updateUserById = async (req, res) => {
  try {
    //B1: Thu thập
    const userId = req.params.userId;
    const reqUser = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: 'userId is invalid'
      })
    }
    if (!reqUser.username && reqUser.username !== undefined) {
      return res.status(400).json({
        message: "username is required",
      });
    }
    if (!reqUser.firstname && reqUser.firstname !== undefined) {
      return res.status(400).json({
        message: "firstname is required",
      });
    }
    if (!reqUser.lastname && reqUser.lastname !== undefined) {
      return res.status(400).json({
        message: "lastname is required",
      });
    }
    //B3: Xử lý
    //tìm và update
    let patchUser = {
      username: reqUser.username,
      firstname: reqUser.firstname,
      lastname: reqUser.lastname,
      password: bcrypt.hashSync(reqUser.password, 8),//cái này tự thêm để login
      roles: reqUser.roles,
    };
    const updatedUser = await db.user.findByIdAndUpdate(userId, patchUser, {new: true});
    if (!updatedUser) {
      return res.status(404).json({
        message: 'User not found'
      })
    }
    //phản hồi
    res.status(200).json({
      message: "Update User By Id successfully",
      data: updatedUser,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "username is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

const deleteUserById = async (req, res) => {
  try {
    //B1: Thu thập
    const userId = req.params.userId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: 'userId is invalid'
      })
    }
    //B3: Xử lý
    //tìm
    const deletedUser = await db.user.findByIdAndDelete(userId);
    if (!deletedUser) {
      return res.status(404).json({
        message: 'User not found'
      })
    }
    //phản hồi
    res.status(200).json({
      message: "Delete User By Id successfully",
      data: deletedUser,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error);
  }
};

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
}